package com.google.firebase.quickstart.database.kotlin.models

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

// [START blog_user_class]
@IgnoreExtraProperties
class User {
    // [START post_to_map]
    var username: String = ""
    var email: String = ""
    var num =0

    constructor()

    constructor(username:String,email:String,num:Int):this(){
        this.username = username
        this.email = email
        this.num = num
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
                "username" to username,
                "email" to email,
                "num" to num
        )
    }
}
